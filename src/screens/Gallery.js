import React, { Component } from 'react'
import Header from '../layout/Header';
import GridView from '../components/Photos/GridView';

export default class Gallery extends Component{
    render(){
        return(
            <>
                <Header />
                <GridView />
            </>
        )
    }
}