import { Key } from "./flickrApi"
var url = `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=${Key}&tags=searchkeyword&tag_mode=searchkeyword&text=searchkeyword&page=customPage&per_page=25&format=json&nojsoncallback=1
`

let timerId = '';

export const deboucingApiCall = (func, delay) => {
    clearTimeout(timerId)
    timerId = setTimeout(func, delay)
}


export const searchPhotos = (tag, page = 1) => {
    
    let framedUrl = url.replace(/searchkeyword/gi, tag)
                    .replace(/customPage/, page)

    return new Promise( (resolve, reject) => {
        if(tag.length){
            fetch(framedUrl)
            .then(res => res.json())
            .then(res => {
                //checking empty array and response stat.
                if(res.stat === "ok" && res.photos.photo.length > 0){
                    //calling fuction for handling and creating url from obj key           
                    resolve(createUrlFromSearchPhotos(res.photos))
                    //.then(photo => window.localStorage.setItem('photos' , JSON.stringify(photo)))

                }else{
                    throw new Error(res.message)
                }
            }).catch((error) => {
                console.log(error)
            })
        }else{
            reject('Enter Some keyphrase!')
        }
    })
}



export const createUrlFromSearchPhotos = (collection) => {
    return new Promise( resolve => {
        resolve(
            collection.photo.map(photo => {
                    return {
                        id : photo.id,
                        title : photo.title,
                        small_image : `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_s.jpg`,
                        thumb_image : `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg`,
                        full_image : `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`                    
                    }
            })
        )
    })
}
