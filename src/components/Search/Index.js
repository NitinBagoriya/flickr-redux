import React,{Component} from 'react'
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { searchPhotos } from './Search';
import { debounce } from 'throttle-debounce';
import { searchFlickerPhotos } from '../../actions/searchActions';

export class SearchBar extends Component{
    constructor(props) {
        super(props);
        //console.log(props)
        this.searchRef = React.createRef();
        this.state = { suggestions : [] }
        this.handleSearchPhotos = debounce(500, this.handleSearchPhotos);
      }

        handleSearchPhotos = (e, query) => {
            //(query == this.props.suggestions.splice[-1]) ? 
              //  this.props.searchFlickerPhotos(query, []) :
             searchPhotos(query).then(data =>  this.props.searchFlickerPhotos(query, data))
        }

        componentDidMount(){
            this.setState({ suggestions : this.props.suggestion })
        }

        shouldComponentUpdate(nextProps, nextState){
            if(JSON.stringify(this.props.suggestions) !== JSON.stringify(this.state.suggestions)){
              this.setState({ suggestions : this.props.suggestions })        
            }    
      
            return true;
        }


    render(){
        return(
            <>  

                <Autocomplete
                    className="search-box"
                    id="combo-box-demo"
                    ref={this.searchRef} 
                    onKeyUp={ (e) => 
                        this.handleSearchPhotos(e, e.target.value)} 
                    onKeyDown={ (e) => this.handleSearchPhotos(e, e.target.value)}
                    onInputChange={ (e,value) => this.handleSearchPhotos(e, value)}
                    type="text"
                    // closeIcon
                    options={this.state.suggestions}
                    getOptionLabel={option => option}
                    renderInput={params => (
                        <TextField {...params} placeholder="Search Here"  variant="outlined" fullWidth  />
                    )}
                />                
            </>
        )
    }
}



function mapStateToProps(state){
    return{
        photos: state.search.photos,
        suggestions : state.search.suggestions
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        searchFlickerPhotos : (payload, photos) => dispatch(searchFlickerPhotos(payload, photos))
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)

