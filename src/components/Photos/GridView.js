  import React,{Component} from 'react';
  import { makeStyles } from '@material-ui/core/styles';
  import Paper from '@material-ui/core/Paper';
  import Grid from '@material-ui/core/Grid';
  import { LazyLoadImage } from 'react-lazy-load-image-component';
  import 'react-lazy-load-image-component/src/effects/blur.css';
  import { connect } from 'react-redux';
  import GridList from '@material-ui/core/GridList';
  import GridListTile from '@material-ui/core/GridListTile';
  import GridModel from './GridModel';
  import Button from '@material-ui/core/Button';
import { searchFlickerPhotosWithPagination } from '../../actions/searchActions';
import { searchPhotos } from '../Search/Search';

  const grdiStyle = {
    btn : { 
      margin : '10px auto',
      padding: '5px 20px',
      color: '#fff',
      background:'#000'
    }
  }


  export class GridView extends Component{
    constructor(props){
      super(props);
  
      this.state = {
        photos : [],
        modelState : false,
        modelTitle : '',
        modelImage : '',
        page : 1
      }

      this.openModel = (image, title) => {
        this.setState({
          modelState : !this.state.modelState,
          modelTitle : title,
          modelImage : image,
        })
      }
  
      this.closeModel = () => {
        this.setState({
          modelState : false,
        })
      }


      this.handleScroll = this.handleScroll.bind(this);
      
    }

    handleScroll() {
      const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
      const body = document.body;
      const html = document.documentElement;
      const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
      const windowBottom = windowHeight + window.pageYOffset;
      if (windowBottom >= docHeight) {
        this.setState({
          page : this.state.page + 1
        })

        searchPhotos(this.props.suggestion.splice(-1), this.state.page)
          .then(data => this.props.searchFlickerPhotosWithPagination(data)).catch( e => alert(e))
      }
    }
  
    componentDidMount() {
      window.addEventListener("scroll", this.handleScroll);
    }

    componentDidUpdate(prevProps, prevState){
      if(JSON.stringify(prevProps.photos) !== JSON.stringify(this.props.photos)){
        // let newPhotos = this.props.photos.filter(e => e !== undefined )
        // let prevPhotos = prevProps.photos.filter(e => e !== undefined )
        this.setState({
          photos : this.props.photos
        })
      }
    }

  
    componentWillUnmount() {
      window.removeEventListener("scroll", this.handleScroll);
    }

  
    render(){
    return (
      <div className={'classes.root grid-view-container'} onScroll={ e => this.handleScroll(e)}>

        {
          (this.state.modelState) ? <GridModel 
                title={this.state.modelTitle} 
                image={this.state.modelImage}
                toggleModel={this.closeModel}
                open={this.state.modelState} 
                /> 
            : ''
        }

        <Grid>
          <Grid item xs={11} style={{
              backgroundColor:'',
              margin:'0 auto',
              padding:'20px 10px'
          }}>
            
              <Grid container spacing={2}>
                  
                  {
                    (this.state.photos.length) ? 
                    
                    this.state.photos.map((photo,key) => {
                      return(
                        <Grid item xs={6} sm={3} key={photo.id}>
                          
                            {/* <p>{photo.small_image}</p> */}
                            <LazyLoadImage
                            alt={photo.title}
                            effect="blur"
                            placeholderSrc='https://loading.io/mod/spinner/camera/index.svg'
                            src={photo.thumb_image} />                 
                          
                          <Button style={ grdiStyle.btn}
                           onClick={ e => this.openModel(photo.full_image,  photo.title) }>View More</Button>
                        </Grid>
                      )
                    })

                    : ' Search Query to fetch Photos '
                  }
                                  
              </Grid>
          
          </Grid>
       </Grid>      
      </div>
    )
    }
  }
  
  function mapStateToProps(state){
    return{
        photos: state.search.photos,
        suggestion : state.search.suggestions
    }
  }

  function mapDispatchToProps(dispatch){
    return{
        searchFlickerPhotosWithPagination : (photos) => dispatch(searchFlickerPhotosWithPagination(photos))
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(GridView)