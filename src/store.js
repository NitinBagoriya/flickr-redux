import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';
//import { composeWithDevTools } from 'redux-devtools-extension';
const localStorage = require('universal-localstorage');

const middleware = [ReduxThunk];


function saveToLocalStorage(state) {
    try {
        const serializedState = JSON.stringify(state.search.suggestions)
        localStorage.setItem('suggestions', serializedState)
    } catch (e) {
        //////console.log(e)
    }
}

function loadFromLocalStorage() {
    try {
        const serializedState = localStorage.getItem('suggestions')
        if (serializedState === null) return undefined
        return JSON.parse(serializedState)
    } catch (e) {
        return undefined
    }
}

const persistedState = loadFromLocalStorage()
////////console.log(persistedState)

const store = createStore(rootReducer, persistedState, 
    applyMiddleware(...middleware) 
);

store.subscribe(() => saveToLocalStorage(store.getState()))

export default store;



