import React,{Component} from 'react'
import SearchBar from '../components/Search/Index';



export default class Header extends Component{
      render(){
        return(
            <div className="header">
                <div className="page-heading">
                    React Flicker Search 
                </div>

                <SearchBar  />
            </div>
        )
    }
}
