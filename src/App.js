import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';
//import { userClick } from './actions/searchActions';


class App extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          {/* <button onClick={() => this.props.userClick('somethingsends')}>Click Here</button> */}
        </header>
      </div>
    );
  }
}


// function mapStateToProps(state){
//   return{
//       todo: state
//   }
// }

// function mapDispatchToProps(dispatch){
//   return{
//        //userClick : (payload) => dispatch(userClick(payload))
//   }
// }

export default App;
