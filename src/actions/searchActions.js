import { SEARCH_FLICKR_PHOTOS, SEARCH_SUGGESTIONS, SEARCH_FLICKR_PHOTOS_PAGINATION  } from "./types";



export const searchFlickerPhotos = (query,photos) => {
    return{
        type : SEARCH_FLICKR_PHOTOS,
        payload : query,
        photos : photos
    }
}


export const searchFlickerPhotosWithPagination = (photos) => {
    return{
        type : SEARCH_FLICKR_PHOTOS_PAGINATION,
        photos : photos
    }
}

export const searchSuggestions = () => {
    return{
        type : SEARCH_SUGGESTIONS,
        payload : ''
    }
}
