import { SEARCH_FLICKR_PHOTOS, SEARCH_FLICKR_PHOTOS_PAGINATION } from "../actions/types";


const initialState =  {
            photos : [],
            //suggestions : []
            suggestions : (window.localStorage.getItem('suggestions')) ? JSON.parse(window.localStorage.getItem('suggestions')) : []
        }    

export default function (state = initialState, action) {
    switch (action.type) {

        case SEARCH_FLICKR_PHOTOS :
            return {
                ...state,
                "photos" : action.photos,
                "suggestions" : [...state.suggestions, action.payload]
            }

        case SEARCH_FLICKR_PHOTOS_PAGINATION :

            return {
                ...state,
                "photos" : [...state.photos, ...action.photos],
                "suggestions" : state.suggestions
            }

        default:
            return state;

    }
}